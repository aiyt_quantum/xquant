'''[根据数据产生不同的交易信号，交易信号的类型分为以下种类：
１、buy,产生一个买入的多头信号
２、sell,平掉多头的信号
３、buy_to_sellshort,卖掉多头并做空
4、sellshort,卖空的信号
5、buy_to_cover,平空的信号
6、sellshort_to_buy,平掉空头并做多
目前，先实现１，２，3,4，5,6这几个基本信号
'''
from collections import deque
from event import SignalEvent
class Strategy(object):
    #策略从tickEvent或者barEvent里面获取数据，并可以考虑决定是否保存数据]
    def __init__(self,data_instance):
        self.asset=data_instance.asset
        self.home_currency=data_instance.home_currency
        self.max_len=data_instance.max_len
        self.money=data_instance.money
        print('strategy_self.money',self.money)
        self.events=data_instance.events
        self.all_pair_name=data_instance.all_pair_name
        self.data_root=data_instance.data_root
        self.trade_pairs=data_instance.trade_pairs
        self.pairs=data_instance.pairs
        self.time_frame=data_instance.time_frame
        self.data_name=data_instance.data_name
        self.symbol_data=data_instance.symbol_data
        self.datas = data_instance.datas
        self.leverage=data_instance.leverage
        self.backtest=data_instance.backtest
        self.signal_id=0
        self.signal_event_dict={}
    #不同的信号类型
    def buy(    self,
                pair,
                size,
                side=None,
                exectype='MARKET',
                price=None,
                limit_price=None,
                take_profit_price=None,
                stop_price=None,
                time=None,
                signal_id=None,
                valid=None,
                transmit=False):
        signal = SignalEvent(
                pair,
                size,
                'buy',
                exectype,
                price,
                limit_price,
                take_profit_price,
                stop_price,
                self.datas[pair]['time'][-1],
                signal_id,
                valid,
                transmit)
        self.signal_event_dict[self.signal_id+1]=signal

    def sell(   self,
                pair,
                size,
                side=None,
                exectype='MARKET',
                price=None,
                limit_price=None,
                take_profit_price=None,
                stop_price=None,
                time=None,
                signal_id=None,
                valid=None,
                transmit=False):
        signal = SignalEvent(
                pair,
                size,
                'sell',
                exectype,
                price,
                limit_price,
                take_profit_price,
                stop_price,
                self.datas[pair]['time'][-1],
                signal_id,
                valid,
                transmit)
        self.signal_event_dict[self.signal_id+1]=signal

    def buy_to_sellshort(self,
                pair,
                size,
                side=None,
                exectype='MARKET',
                price=None,
                limit_price=None,
                take_profit_price=None,
                stop_price=None,
                time=None,
                signal_id=None,
                valid=None,
                transmit=False):
        signal = SignalEvent(
                pair,
                size,
                'buy_to_sellshort',
                exectype,
                price,
                limit_price,
                take_profit_price,
                stop_price,
                self.datas[pair]['time'][-1],
                signal_id,
                valid,
                transmit)
        self.signal_event_dict[self.signal_id+1]=signal

    def sellshort(    self,
                pair,
                size,
                side=None,
                exectype='MARKET',
                price=None,
                limit_price=None,
                take_profit_price=None,
                stop_price=None,
                time=None,
                signal_id=None,
                valid=None,
                transmit=False):
        signal = SignalEvent(
                pair,
                size,
                'sellshort',
                exectype,
                price,
                limit_price,
                take_profit_price,
                stop_price,
                self.datas[pair]['time'][-1],
                signal_id,
                valid,
                transmit)
        self.signal_event_dict[self.signal_id+1]=signal

    def buy_to_cover(self,
                pair,
                size,
                side=None,
                exectype='MARKET',
                price=None,
                limit_price=None,
                take_profit_price=None,
                stop_price=None,
                time=None,
                signal_id=None,
                valid=None,
                transmit=False):
        signal = SignalEvent(
                pair,
                size,
                'buy_to_cover',
                exectype,
                self.datas[pair]['time'][-1],
                limit_price,
                take_profit_price,
                stop_price,
                time,
                signal_id,
                valid,
                transmit)
        self.signal_event_dict[self.signal_id+1]=signal

    def sellshort_to_buy(self,
                pair,
                size,
                side=None,
                exectype='MARKET',
                price=None,
                limit_price=None,
                take_profit_price=None,
                stop_price=None,
                time=None,
                signal_id=None,
                valid=None,
                transmit=False):
        signal = SignalEvent(
                pair,
                size,
                'sellshort_to_buy',
                exectype,
                price,
                limit_price,
                take_profit_price,
                stop_price,
                self.datas[pair]['time'][-1],
                signal_id,
                valid,
                transmit)
        self.signal_event_dict[self.signal_id+1]=signal





