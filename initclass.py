import queue
from info import _INSTRUMENTS_
class InitClass(object):
    #初始化参数，到init类里面
    def __init__(self,
                trade_pairs,
                time_frame,
                data_root='/opt/data/fx/',
                asset='currency',
                home_currency='USD',
                leverage=50,
                money=1000000,
                backtest=True,
                max_len=500):
        self.asset=asset
        self.home_currency=home_currency
        self.money=money
        print('init_calss_self.money',self.money)
        self.max_len=max_len
        self.events=queue.Queue()
        self.all_pair_name=list(_INSTRUMENTS_.keys())
        self.data_root=data_root
        self.time_frame=time_frame
        self.trade_pairs=trade_pairs
        self.continue_backtest=True
        self.leverage=leverage
        self.backtest=True