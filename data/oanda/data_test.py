import queue
import threading
import sys
import time
sys.path.insert(0,'/home/yjj/xquant/')
sys.path.insert(0,'/opt/xquant/')
from livetickdata import TickPrices
from livebardata import BarPrices
from data.historycsvbar import HistoryBarData

ACCESS_TOKEN = 'a47a9148c4b16a2fcb1f87753d671e04-ce688ecf7704a36103e4014dd29c5c0f'
ACCOUNT_ID = '101-011-6808069-003'
global exit_flag
def load_data(events,history_prices):
    tick_count=0
    bar_count=0
    iters = 0
    while iters < 10000000 and history_prices.continue_backtest:
            iters+=1
            try:
                event = events.get(False)
            except queue.Empty:
                history_prices._to_queue()
            else:
                if event is not None:

                    if event.type == 'TICK':
                        print('tick数据来了')
                        print('instrument',event.instrument)
                        print('time',event.time)
                        print('bid',event.bid)
                        print('ask',event.ask)
                        tick_count+=1
                        print('tick数据的次数',tick_count)
                    if event.type == 'BAR':
                        print('bar数据来了')
                        print('instrument',event.instrument)
                        print('time',event.time)
                        print('o',event.o)
                        print('h',event.h)
                        bar_count+=1
                        print('bar数据的次数',bar_count)
                        #time.sleep(5)


def test_data():
    events = queue.Queue()
    #if Price_type==TickPrices:
    tick_prices = TickPrices(
            'practice', ACCESS_TOKEN, ACCOUNT_ID,
            'EUR_USD,AUD_JPY,USD_JPY', events
        )

    #elif Price_type==BarPrices:
    bar_prices = BarPrices(
            'practice', ACCESS_TOKEN, ACCOUNT_ID,
            'EUR_USD', 'M1',events
        )

    trade_thread = threading.Thread(target=load_data, args=(events,))
    tick_price_thread = threading.Thread(target=tick_prices._to_queue, args=[])
    bar_price_thread= threading.Thread(target=bar_prices._to_queue, args=[])

    # Start both threads
    trade_thread.start()
    tick_price_thread.start()
    bar_price_thread.start()

def test_hishtory_bar(symbol_list, csv_dir):
    events = queue.Queue()
    #if Price_type==TickPrices:
    history_prices = HistoryBarData(
            symbol_list,csv_dir,events
        )
    load_data(events,history_prices)
    print('all over')

if __name__ == '__main__':
    #test_data()
    test_hishtory_bar(['EUR_USD'],'/opt/data/fx/oanda_60M/')

